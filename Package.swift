// swift-tools-version: 5.7
// The swift-tools-version declares the minimum version of Swift required to build this package.
import PackageDescription

let package = Package(
  name: "Element",
  dependencies: [
  ],
  targets: [
    .executableTarget(name: "Element", dependencies: []),
    .testTarget(name: "ElementTests", dependencies: ["Element"]),
  ]
)
