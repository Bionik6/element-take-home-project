# Element Cron Parser

# Description:
This is a Swift Package Manager project to parse simple crons like `45 * /bin/run_me_hourly 12:30` or `* * /bin/run_me_every_minute 4:38`  

# Usage:
* Download the project
* Open it in Xcode 
* Build and run it 
* Type the command you want to evaluate. The format should be `cron script desired_time`, eg: `45 * /bin/run_me_every_minute 15:40` 
* Watch the program shows you when the script is going to run   

# Things to improve:
Since the project shouldn't take more than 2hours to complete, there are some things remaining
* Writing the tests suite for the whole app 
* Build a shell script to run it system wide like `cat input.txt | swift run element 15:25`
* Make the parser more scalable. Since we're dealing with only hours and minutes the current implementation should be fine
