/**
 *  Element
 *  Element.swift
 *  Copyright (c) Ibrahima Ciss 2022
 */

import Foundation

@main
public struct Element {
  public private(set) var text = "Welcome to Element Cron Parser"
  
  public static func main() {
    print("Welcome to Element Cron Parser, please enter the cron you want to evaluate.")
    print("The format should be: `cron script desired_time`, eg: `45 * /bin/run_me_every_minute 15:40`")
    let command = ElementConsole.checkReadLine()
    do {
      let cron = try ElementCronParser(command: command)
      let nextExecutionDate = try cron.getNextDate()
      print(ElementTimeFormatter.getDateString(from: nextExecutionDate, script: cron.script))
    } catch {
      guard let localizedError = error as? LocalizedError else {
        print("Oops, something bad happens", error.localizedDescription)
        return
      }
      print(localizedError.errorDescription ?? "", "-", localizedError.recoverySuggestion ?? "")
    }
  }
  
  @discardableResult
  static func cmd(_ command: String, _ arguments: String...) -> Int32 {
    let stdout = Pipe()
    let task = Process()
    
    task.standardOutput = stdout
    task.executableURL = URL(fileURLWithPath: "/usr/bin/env")
    task.arguments = [command] + arguments
    task.launch()
    
    let data = stdout.fileHandleForReading.readDataToEndOfFile()
    let output = String(data: data, encoding: .utf8)!
    
    print(output)
    
    return task.terminationStatus
  }
  
}
