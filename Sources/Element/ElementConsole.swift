/**
 *  Element
 *  ElementConsole.swift
 *  Copyright (c) Ibrahima Ciss 2022
 */

import Foundation


/// Helper file for the console
struct ElementConsole {
  
  /// Get the input from the shell
  /// - Returns: The command entered in the shell
  static func checkReadLine() -> String {
    guard let line = readLine() else {
      print("No input, please provide one")
      return checkReadLine()
    }
    return line.trimmingCharacters(in: .whitespaces)
  }
  
}
