/**
 *  Element
 *  ElementCronParser.swift
 *  Copyright (c) Ibrahima Ciss 2022
 */

import Foundation

/// Cron parser.
///
/// You can parse expression like
/// `45 * /bin/run_me_hourly 1:30`
/// and it will tell you the time this cron is going to run.
struct ElementCronParser {
  
  /// The given script
  private(set) var script: String
  
  /// The cron expression
  private let expression: String
  
  /// The time we want to execute the cron
  private let desiredTime: String
  
  
  /// Parser initializer
  /// - Parameter command: the command we want to evaluate
  init(command: String) throws {
    guard ElementCronParser.isValidCron(command) else {
      throw ElementCronParserError.formatNotValid
    }
    let parts = ElementCronParser.getParts(from: command)
    self.script = parts.script
    self.expression = parts.expression
    self.desiredTime = parts.desiredTime
  }
  
  /// Evaluate and get the time the cron should run
  /// - Returns: the exact time the cron is about to run
  func getNextDate() throws -> Date {
    let timeParts = desiredTime.components(separatedBy: ":")
    guard timeParts.count == 2 else {
      throw ElementCronParserError.timeFormatNotValid
    }
    
    let calendar = Calendar.current
    var components = calendar.dateComponents([.day, .hour, .minute, .month, .year], from: Date())
    components.hour = Int(timeParts[0])
    components.minute = Int(timeParts[1])
    
    guard let desiredDate = calendar.date(from: components) else {
      throw ElementCronParserError.timeFormatNotValid
    }
    
    if expression == "* *" { return desiredDate } // * * /bin/run_me_every_minute
    
    let parts = expression.components(separatedBy: " ")
    let minutes = parts[0]
    let hour = parts[1]
    
    if minutes != "*" && hour != "*" { // 30 1 /bin/run_me_daily
      components.hour = Int(hour)
      components.minute = Int(minutes)
      let date = calendar.date(from: components) ?? Date()
      if desiredDate > date {
        let executionDate = calendar.date(byAdding: .day, value: 1, to: date)
        return executionDate ?? Date()
      }
      return date
    }
    
    if minutes != "*" && hour == "*" { // 45 * /bin/run_me_hourly
      components.minute = Int(minutes)
      components.hour = calendar.component(.hour, from: desiredDate)
      let date = calendar.date(from: components) ?? Date()
      if desiredDate > date {
        let executionDate = calendar.date(byAdding: .hour, value: 1, to: date)
        return executionDate ?? Date()
      }
      return date
    }
    
    if minutes == "*" && hour != "*" { // * 19 /bin/run_me_sixty_times
      components.minute = 00
      components.hour = Int(hour)
      let date = calendar.date(from: components) ?? Date()
      if desiredDate > date {
        let executionDate = calendar.date(byAdding: .day, value: 1, to: date)
        return executionDate ?? Date()
      }
      return date
    }
    
    return Date()
  }
  
  /// Verify cron validity
  /// - Parameter command: The string we receive
  /// - Returns: a boolean indicating wheter the cron is valid or not
  static func isValidCron(_ command: String) -> Bool {
    let parts = command.split(separator: " ")
    guard parts.count == 4 else { return false }
    guard checkNumberValidity(of: "\(parts[0])", range: 0...59),
          checkNumberValidity(of: "\(parts[1])", range: 0...23),
          parts[2].prefix(1) == "/"
    else { return false }
    return true
  }
  
}


// MARK: - Private APIs
extension ElementCronParser {
  
  /// Method to get the different parts of the command
  /// - Parameter command: The string we received as a command
  /// - Returns: A tuple containing each part of the command
  private static func getParts(from command: String) -> (expression: String, script: String, desiredTime: String) {
    let parts = command.split(separator: " ")
    return ("\(parts[0]) \(parts[1])", "\(parts[2])", "\(parts[3])")
  }
  
  /// Check the validity of the time entered
  /// - Parameter string: The time in string
  /// - Returns: a boolean indicating whether the time is valid or not
  private static func checkTimeValidity(of string: String) -> Bool {
    if string == "*" { return true }
    if Int(string) != nil { return true }
    return false
  }
  
  /// Check if the given cron time (hour and/or minute) is valid
  /// - Parameters:
  ///   - string: The give time
  ///   - range: The range depending on hour or minute
  /// - Returns: a boolean indicating whether the time is valid or not
  private static func checkNumberValidity(of string: String, range: ClosedRange<Int>) -> Bool {
    if string == "*" { return true }
    if let number = Int(string), range ~= number {
      return true
    }
    return false
  }
}
