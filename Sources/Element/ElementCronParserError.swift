/**
 *  Element
 *  ElementCronParserError.swift
 *  Copyright (c) Ibrahima Ciss 2022
 */

import Foundation

/// CronParser errors
enum ElementCronParserError: LocalizedError {
  case formatNotValid
  case timeFormatNotValid
  
  var errorDescription: String? {
    switch self {
      case .formatNotValid:
        return "This seems to be an invalid cron expression."
      case .timeFormatNotValid:
        return "The given time seems to be invalid"
    }
  }
  
  var recoverySuggestion: String? {
    switch self {
      case .formatNotValid:
        return "Please provide a valid format to be parsed, eg: `45 * /bin/run_me_hourly 10:20`"
      case .timeFormatNotValid:
        return "Please provide a valid time, eg: `14:30`"
    }
  }
  
}
