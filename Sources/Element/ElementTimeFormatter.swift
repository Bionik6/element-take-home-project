/**
 *  Element
 *  ElementTimeFormatter.swift
 *  Copyright (c) Ibrahima Ciss 2022
 */

import Foundation

/// Element time formatter
struct ElementTimeFormatter {
  
  /// Format the input to a human readable string like: `16:10 today - /bin/run_me_every_minute`
  /// - Parameters:
  ///   - date: The date to be formatted
  ///   - script: The script to run
  /// - Returns: A string representing the exact date and time the cron should run
  static func getDateString(from date: Date, script: String) -> String {
    let calendar = Calendar.current
    
    let formatter = DateFormatter()
    formatter.dateFormat = "H:mm"
    let time = formatter.string(from: date)
    
    if calendar.isDateInToday(date) {
      return "\(time) today - \(script)"
    } else {
      return "\(time) tomorrow - \(script)"
    }
  }
  
}
