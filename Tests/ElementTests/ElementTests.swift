import XCTest
@testable import Element

final class ElementTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(Element().text, "Welcome to Element Cron Parser")
    }
}
